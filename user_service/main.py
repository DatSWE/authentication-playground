from fastapi import FastAPI, Depends, HTTPException, Request, status
from fastapi.middleware.cors import CORSMiddleware
import os
from models import AccountOut, AccountIn, Account, AccountToken
from queries import AccountRepo
from authenticator import MyAuthenticator
from models import AuthenticationException

authenticator = MyAuthenticator(os.environ["SIGNING_KEY"])
app = FastAPI()
app.include_router(authenticator.router)

app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ.get("CORS_HOST", "http://localhost:3000")],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def health_check():
    return {"Hello": "World"}


@app.get("/api/user/{pk}")
async def get_user(
    pk: int,
    accounts: AccountRepo = Depends(),
    ra=Depends(authenticator.get_current_account_data),
) -> AccountOut:
    try:
        account = accounts.get_user_by_id(pk)
    except AuthenticationException:
        return HTTPException(status.HTTP_401_UNAUTHORIZED)
    return account


@app.post("/api/user")
def create_user(
    info: AccountIn,
    accounts: AccountRepo = Depends(),
) -> AccountOut:
    hashed_password = authenticator.hash_password(info.password)
    ar = Account(
        username=info.username,
        first=info.first,
        last=info.last,
        age=info.age,
        email=info.email,
    )
    try:
        pk = accounts.create_user(ar, hashed_password)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(e),
        )
    return accounts.get_user_by_id(pk)


@app.get("/token")
async def get_by_cookie(
    request: Request,
    account_data: dict
    | None = Depends(authenticator.try_get_current_account_data),
    accounts: AccountRepo = Depends(),
    ra=Depends(authenticator.get_current_account_data),
) -> AccountToken:
    account = await get_user(account_data["id"], accounts=accounts, ra=ra)
    return {
        "access_token": request.cookies[authenticator.cookie_name],
        "type": "Bearer",
        "account": account,
    }
