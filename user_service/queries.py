from psycopg_pool import ConnectionPool
import os
from models import AccountOut, Account
from models import AuthenticationException

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class AccountRepo:
    def get_user_by_id(self, pk: int) -> AccountOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, username, first_name, last_name,
                        modified, age, email, password_hash
                    FROM accounts
                    WHERE id = %s;
                    """,
                    [pk],
                )
                ac = cur.fetchone()
                if ac is None:
                    raise AuthenticationException("No account found")
                else:
                    try:
                        return AccountOut(
                            id=ac[0],
                            username=ac[1],
                            first=ac[2],
                            last=ac[3],
                            modified=ac[4].isoformat(),
                            age=ac[5],
                            email=ac[6],
                            hashed_password=ac[7],
                        )
                    except Exception as e:
                        raise Exception("Error:", e)

    def get(self, username: str) -> AccountOut | None:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, username, first_name, last_name,
                        modified, age, email, password_hash
                    FROM accounts
                    WHERE username = %s;
                    """,
                    [username],
                )
                ac = cur.fetchone()
                if ac is None:
                    raise Exception("No account found")
                else:
                    try:
                        return AccountOut(
                            id=ac[0],
                            username=ac[1],
                            first=ac[2],
                            last=ac[3],
                            modified=ac[4].isoformat(),
                            age=ac[5],
                            email=ac[6],
                            hashed_password=ac[7],
                        )
                    except Exception as e:
                        raise Exception("Error:", e)

    def create_user(self, account: Account, hashed_password: str) -> int:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    INSERT INTO accounts
                    (username, password_hash, first_name,
                        last_name, age, email)
                    VALUES (%s, %s, %s, %s, %s, %s)
                    RETURNING ID;
                    """,
                    (
                        account.username,
                        hashed_password,
                        account.first,
                        account.last,
                        account.age,
                        account.email,
                    ),
                )
                pk = cur.fetchone()[0]
                return pk

    def delete_user(self, pk: int) -> AccountOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    DELETE FROM accounts
                    WHERE id = %s
                    RETURNING *;
                    """,
                    (pk),
                )
